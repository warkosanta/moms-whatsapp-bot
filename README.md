See more: https://www.notion.so/Mom-s-WhatsApp-Bot-0aaccfc795134536873b46bfa21cfcc8

Used:

* ASP.NET Core MVC
* YouTube API
* Twilio service

Deploy:

* Docker
* Heroku
  
Database:

* NoSql MongoDB  
